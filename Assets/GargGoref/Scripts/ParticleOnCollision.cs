﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GargGoref
{
	public class ParticleOnCollision : MonoBehaviour
	{
		public ParticleType particleType;
		public bool requireMinimumVelocity = true;
		public bool rotateRandomlyOnZAxis = true;
		public bool influencedByVelocity;
		public bool occasional;
		public ParticleMaster primeMinister;
		public ParticleSystem thisSystem;
		Transform t;
		Vector3 velocity;
		Vector3 lastPos;
		void FixedUpdate()
		{
			if (influencedByVelocity)
			{
				if (!t)
				{
					Rigidbody r = GetComponentInParent<Rigidbody>();
					if (r)
						t = r.transform;
				}
				else
				{
					velocity = lastPos - t.position;
					lastPos = t.position;
				}
			}
		}
		void OnParticleCollision(GameObject collided)
		{
			if (influencedByVelocity && !t)
				return;
			
			Vector3 intersection;
			Vector3 normal;
			
			var pces = new List<ParticleCollisionEvent>();
			thisSystem.GetCollisionEvents(collided, pces);
			for (int i = 0; i < pces.Count; i++)
			{
				if (primeMinister && (!occasional || Random.Range(0, 2) == 0) && (!requireMinimumVelocity || BloodyHellLad.NonEuclideanSqrMagnitude(pces[i].velocity) > .1f))
				{
					intersection = pces[i].intersection;
					normal = pces[i].normal;
					
					Quaternion rot = influencedByVelocity ? Quaternion.LookRotation(normal, velocity) : Quaternion.LookRotation(-normal);
					
					primeMinister.EmitAt(intersection, rotateRandomlyOnZAxis ? Quaternion.Euler(rot.eulerAngles + Vector3.forward * Random.Range(0, 360)) : rot, (int)particleType);
				}
			}
		}
	}
}