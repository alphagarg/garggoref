﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace GargGoref
{
	public class BloodPooling : MonoBehaviour
	{
		public ParticleMaster pm;
		Vector3 lastPosition;
		Vector3 lastPool;
		Transform t;
		
		async void Start()
		{
			t = transform;
			
			var s = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
			while (t && s == UnityEngine.SceneManagement.SceneManager.GetActiveScene())
			{
				TimedUpdate();
				
				await Task.Delay(Random.Range(1000,3000));
			}
		}
		void TimedUpdate()
		{
			if (
				(lastPool - t.position).sqrMagnitude > 1 && //Don't spawn two pools in the same spot!
				(lastPosition - t.position).sqrMagnitude < .01f //Make sure we haven't moved recently
			)
			{
				pm.EmitAt(t.position, t.rotation, (int)ParticleType.pooler);
				lastPool = t.position;
			}
			
			lastPosition = t.position;
		}
	}
}