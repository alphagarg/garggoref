﻿using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;

namespace GargGoref
{
	public enum ParticleType
	{
		decalSmear = 0,
		decalPool = 1,
		decalDroplet = 2,
		decalSplatter = 3,
		gibBrain = 4,
		gibBone = 5,
		gibFlesh = 6,
		droplet = 7,
		spurt = 8,
		squirt = 9,
		puff = 10,
		puffyPuff = 11,
		smearer = 12,
		bleeding = 13,
		pooler = 14,
		sparks = 15
	}
	public class ParticleMaster : MonoBehaviour
	{
		public ParticleSystem[] systems;
		[Range(0,1)]
		public float maxParticleMultiplier = 1;
		
		public ParticleSystem EmitAt(Vector3 point, Quaternion rotation, int index, int count, Transform parent)
		{
			ParticleSystem ps = Instantiate(systems[index].gameObject, parent).GetComponent<ParticleSystem>();
			
			EmitAt(ps, point, rotation, count);
			
			return ps;
		}
		public void EmitAt(Vector3 point, Quaternion rotation, int index, int count = 1)
		{
			ParticleSystem ps = systems[index];
			
			EmitAt(ps, point, rotation, count);
		}
		public void EmitAt(ParticleSystem ps, Vector3 point, Quaternion rotation, int count = 1)
		{
			ps.transform.position = point;
			ps.transform.rotation = rotation;
			
			float startTime = Time.time;
			float timeMult = 1.0f / ps.main.duration;
			
			ps.startRotation3D = rotation.eulerAngles * Mathf.Deg2Rad;
			ps.Emit(count);
		}
		void FixedUpdate()
		{
			foreach (var s in systems)
			{
				if (s.particleCount >= s.main.maxParticles * maxParticleMultiplier - 10)
				{
					var p = new ParticleSystem.Particle[s.main.maxParticles];
					s.GetParticles(p);
					
					List<ParticleSystem.Particle> pp = new List<ParticleSystem.Particle>();
					for (int i = p.Length - 1; i >= 0; i--)
						if (Random.Range(0, 10) != 0)
							pp.Add(p[i]);
					
					s.SetParticles(pp.ToArray());
				}
			}
		}
	}
}