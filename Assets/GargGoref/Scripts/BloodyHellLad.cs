﻿#define USE_GARGGORE_DAMAGE_SYSTEM
#define USE_GARGGORE_BLEEDING_SYSTEM
#define USE_GARGGORE_ARMOUR_SYSTEM

using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;

namespace GargGoref
{
	[System.Serializable]
	public class ArmourBoundsHelper
	{
		public Collider bounds;
		public bool active = true;
	}
	[System.Serializable]
	public class DismembermentHelper
	{
		public Transform limb;
		
		[HideInInspector]
		public GameObject stubble;
	}
	[System.Serializable]
	public class BoolPtr
	{
		public bool value;
	}
	public class BloodyHellLad : MonoBehaviour
	{
		public virtual ushort bloodVolume
		{
			get { return _bloodVolume; }
			set
			{
				OnBloodVolumeChanged((short)(_bloodVolume - value));
				_bloodVolume = value;
			}
		}
		public virtual byte health
		{
			get { return _health; }
			set
			{
				OnHealthChanged((sbyte)(_health - value));
				_health = value;
			}
		}
		ushort _bloodVolume = 5000; //5000ml
		byte _health = 100;
		//In case you still wanna use the bleeding system here and just transfer the changes to your own or something
		public virtual void OnBloodVolumeChanged(short difference)
		{
			
		}
		//Ditto, but for the damage system
		public virtual void OnHealthChanged(sbyte difference)
		{
			
		}
		
		//For when a projectile hits one of the critical damage areas
		public virtual void OnFemurHit(float intensity)
		{
			
		}
		public virtual void OnBrainHit(float intensity)
		{
			
		}
		public virtual void OnHeartHit(float intensity)
		{
			
		}
		public virtual void OnArteryHit(float intensity)
		{
			
		}
		public virtual void OnDismember(float intensity)
		{
			
		}
		
		public static ParticleMaster pm;
		public DismembermentHelper[] dismembermentUtils;
		public GameObject stubble;
		public Collider femurBounds;
		public Collider brainBounds;
		public Collider heartBounds;
		public Collider[] arteryBounds;
		public ArmourBoundsHelper[] armourBounds;
		public List<Transform> bleeding;
		
		public SkinnedMeshRenderer[] characterRenderers;
		protected Transform ct;
		
		protected virtual async void Start()
		{
			foreach (SkinnedMeshRenderer character in characterRenderers)
				character.sharedMesh = Instantiate(character.sharedMesh);
			ct = characterRenderers[0].transform;
			if (!pm)
				pm = FindObjectOfType<ParticleMaster>();
			
			foreach (var e in dismembermentUtils)
			{
				e.stubble = Instantiate(stubble, e.limb);
				e.stubble.transform.parent = e.limb.parent;
				e.stubble.transform.rotation = Quaternion.LookRotation(-e.limb.right);
				e.stubble.SetActive(false);
			}
			
			randy = new System.Random();
			
			#if USE_GARGGORE_BLEEDING_SYSTEM
			var s = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
			while (s == UnityEngine.SceneManagement.SceneManager.GetActiveScene())
			{
				await Task.Delay(2000);
				
				TimedUpdate();
			}
			
		}
		protected virtual void TimedUpdate()
		{
			bloodVolume -= (ushort)Mathf.Clamp(bleeding.Count, 0, bloodVolume * 1.0f);
			
			if (bloodVolume <= bleeding.Count * 10 && bleeding.Count > 0)
			{
				int r = Random.Range(0, bleeding.Count);
				Destroy(bleeding[r].gameObject);
				bleeding.RemoveAt(r);
			}
			#endif
		}
		
		public virtual void Wound(Vector3 point, float intensity, int vertexCount, Vector3[] vertices, List<Color> vc, BoolPtr wd = null)
		{
			float sqrInt = intensity*intensity*9;
			for (int i = vertexCount - 1; i >= 0; i--)
			{
				float sqrDist = NonEuclideanSqrMagnitude(vertices[i] - point);
				if (sqrDist < sqrInt)
				{
					vc[i] -= Color.black * Mathf.Clamp((sqrInt - sqrDist) * RandomRange(6, 18), 0, vc[i].a); //Add vertex colour alpha to turn innards to outtards
					if (wd != null)
						wd.value |= vc[i].a < 0.1f;
				}
			}
		}
		/// <summary>
		/// Has an entry and exit wound, the latter of which is larger than the former.
		/// Has locational context sensitive particle effects (e.g. brain gibs on brain hits)
		/// Can activate dismemberment.
		/// Activates bleeding.
		/// 
		/// Useful for projectile damage, i.e. bullets and shrapnel.
		/// </summary>
		/// <param name="point">Hit point.</param>
		/// <param name="direction">Hit direction.</param>
		/// <param name="intensity">A 2000th of the damage you wish to deal to the character's health parametre.</param>
		/// <param name="rbody">Rigidbody of the limb that was hit, as this system is made for active ragdolls.</param>
		public virtual async void VelocityWound(Vector3 point, Vector3 direction, float intensity, Rigidbody rbody)
		{
			Ray bulletRay = new Ray(point, direction);
			#if USE_GARGGORE_DAMAGE_SYSTEM
			int damageMultiplier = 2000;
			#endif
			#if USE_GARGGORE_ARMOUR_SYSTEM
			foreach (var e in armourBounds)
			{
				if (e.active && e.bounds.bounds.IntersectRay(bulletRay))
				{
					pm.EmitAt(e.bounds.ClosestPointOnBounds(point), Quaternion.LookRotation(-direction), (int)ParticleType.sparks, 10);
					intensity*=.9f;
				}
			}
			#endif
			
			bleeding.Add(pm.EmitAt(point, Quaternion.identity, (int)ParticleType.bleeding, 1, rbody.transform).transform);
			
			pm.EmitAt(point, Quaternion.LookRotation(-direction), (int)ParticleType.spurt);
			pm.EmitAt(point, Quaternion.Euler(Vector3.one * Random.Range(0, 360)), (int)ParticleType.puff);
			pm.EmitAt(point, Quaternion.Euler(Vector3.one * Random.Range(0, 360)), (int)ParticleType.puffyPuff);
			
			pm.EmitAt(point, Quaternion.identity, (int)ParticleType.smearer, 1, rbody.transform);
			
			pm.EmitAt(point, Quaternion.LookRotation(direction), (int)ParticleType.droplet, (int)(Random.Range(1, intensity * 2000 + 2)));
			
			if (brainBounds.bounds.IntersectRay(bulletRay))
			{
				OnBrainHit(intensity);
				#if USE_GARGGORE_DAMAGE_SYSTEM
				damageMultiplier*=8;
				#endif
				pm.EmitAt(point, Quaternion.LookRotation(direction), (int)ParticleType.gibBrain, (int)Random.Range(1, intensity * 500 + 2));
			}
			else if (heartBounds.bounds.IntersectRay(bulletRay))
			{
				OnHeartHit(intensity);
				#if USE_GARGGORE_DAMAGE_SYSTEM
				damageMultiplier*=8;
				#endif
				pm.EmitAt(point, Quaternion.LookRotation(-direction), (int)ParticleType.squirt, 1, rbody.transform);
			}
			else
			{
				if (femurBounds.bounds.IntersectRay(bulletRay))
				{
					OnFemurHit(intensity);
					#if USE_GARGGORE_DAMAGE_SYSTEM
					damageMultiplier*=4;
					#endif
					pm.EmitAt(point, Quaternion.LookRotation(direction), (int)ParticleType.gibBone, (int)Random.Range(1, intensity * 500 + 2));
				}
				foreach (var e in arteryBounds)
				{
					if (e.bounds.IntersectRay(bulletRay))
					{
						OnArteryHit(intensity);
						#if USE_GARGGORE_DAMAGE_SYSTEM
						damageMultiplier*=2;
						#endif
						if (Random.Range(0, 5) == 0)
							pm.EmitAt(point, Quaternion.LookRotation(-direction), (int)ParticleType.squirt, 1, rbody.transform);
					}
				}
			}
			
			#if USE_GARGGORE_DAMAGE_SYSTEM
			float sqrInt = intensity*intensity*9 * damageMultiplier;
			health -= (byte)Mathf.Clamp(sqrInt, 0, health * 1.0f);
			#if USE_GARGGORE_BLEEDING_SYSTEM
			bloodVolume -= (ushort)Mathf.Clamp(sqrInt, 0, bloodVolume * 1.0f);
			#endif
			rbody.AddForceAtPosition(direction * sqrInt * 24, point);
			#endif
			
			if (health < 75)
				pm.EmitAt(point, Quaternion.LookRotation(direction), (int)ParticleType.gibFlesh, (int)Random.Range(1, intensity * 500 + 2));
			
			
			
			Vector3 ogPoint = point; point = ct.InverseTransformPoint(point); var wd = new BoolPtr();
			RaycastHit hit;
			Vector3 exitPoint = point;
			if (Physics.Raycast(ogPoint + direction * 2, -direction, out hit, 2, (1 << ct.gameObject.layer)))
			{
				exitPoint = ct.InverseTransformPoint(hit.point);
				pm.EmitAt(hit.point, Quaternion.identity, (int)ParticleType.smearer, 1, rbody.transform);
			}
			Transform rbt = rbody.transform;
			ogPoint = rbt.InverseTransformPoint(ogPoint);
			
			foreach (SkinnedMeshRenderer character in characterRenderers)
			{
				Mesh m = new Mesh();
				character.BakeMesh(m);
				
				List<Color> vc = new List<Color>();
				character.sharedMesh.GetColors(vc);
				if (vc.Count == 0)
				{
					vc = new List<Color>(new Color[m.vertexCount]);
					for (int i = vc.Count - 1; i >= 0; i--)
						vc[i] = Color.white;
				}
				
				//Entrance and exit
				int vertexCount = m.vertexCount;
				Vector3[] vertices = m.vertices;
				
				await Task.Run(() => Wound(point, intensity, vertexCount, vertices, vc, wd));
				await Task.Run(() => Wound(exitPoint, intensity * 2, vertexCount, vertices, vc, wd));
				
				character.sharedMesh.SetColors(vc);
			}
			
			ogPoint = rbt.TransformPoint(ogPoint);
			if (wd.value && intensity * 2000 > 100)
			{
				foreach (var e in dismembermentUtils)
				{
					if (!e.stubble.activeInHierarchy && (e.limb.position - ogPoint).sqrMagnitude < intensity * .125f)
					{
						#if USE_GARGGORE_BLEEDING_SYSTEM
						bloodVolume -= (ushort)Mathf.Clamp(Random.Range(0, 2000), 0, bloodVolume * 1.0f);
						#endif
						
						OnDismember(intensity);
						
						e.stubble.SetActive(true);
						e.limb.gameObject.SetActive(false);
						e.limb.localScale = Vector3.zero;
						
						pm.EmitAt(ogPoint, Quaternion.identity, (int)ParticleType.puff);
						pm.EmitAt(ogPoint, Quaternion.identity, (int)ParticleType.puffyPuff);
						pm.EmitAt(ogPoint, Quaternion.identity, (int)ParticleType.smearer, 1, e.limb.parent);
						
						pm.EmitAt(ogPoint, Quaternion.LookRotation(-e.limb.right), (int)ParticleType.spurt);
						pm.EmitAt(ogPoint, Quaternion.LookRotation(-e.limb.right + Vector3.up), (int)ParticleType.gibFlesh, 16);
					}
				}
			}
		}
		/// <summary>
		/// Only has one wound that's calculated from a point, with no directional data.
		/// Cannot activate dismemberment.
		/// Does not activate bleeding.
		/// 
		/// Useful for physics damage or melee damage.
		/// </summary>
		/// <param name="point">Hit point.</param>
		/// <param name="intensity">A 2000th of the damage you wish to deal to the character's health parametre.</param>
		/// <param name="rbody">Rigidbody of the limb that was hit, as this system is made for active ragdolls.</param>
		public virtual async void BluntWound(Vector3 point, float intensity, Rigidbody rbody)
		{
			Vector3 direction = (rbody.position - point).normalized;
			
			pm.EmitAt(point, Quaternion.identity, (int)ParticleType.smearer, 1, rbody.transform);
			
			pm.EmitAt(point, Quaternion.LookRotation(direction), (int)ParticleType.droplet, (int)(Random.Range(0, intensity * 2000 + 2)));
			
			#if USE_GARGGORE_DAMAGE_SYSTEM
			float sqrInt = intensity*intensity*9 * 2000;
			health -= (byte)Mathf.Clamp(sqrInt, 0, health * 1.0f);
			#if USE_GARGGORE_BLEEDING_SYSTEM
			bloodVolume -= (ushort)Mathf.Clamp(sqrInt, 0, bloodVolume * 1.0f);
			#endif
			rbody.AddForceAtPosition(direction * sqrInt * 24, point);
			#endif
			
			if (health < 75)
				pm.EmitAt(point, Quaternion.LookRotation(direction), (int)ParticleType.gibFlesh, (int)(Random.Range(0, intensity * 500 + 2)));
			
			
			
			Vector3 ogPoint = point; point = ct.InverseTransformPoint(point); var wd = new BoolPtr();
			
			foreach (SkinnedMeshRenderer character in characterRenderers)
			{
				Mesh m = new Mesh();
				character.BakeMesh(m);
				
				List<Color> vc = new List<Color>();
				character.sharedMesh.GetColors(vc);
				if (vc.Count == 0)
				{
					vc = new List<Color>(new Color[m.vertexCount]);
					for (int i = vc.Count - 1; i >= 0; i--)
						vc[i] = Color.white;
				}
				
				int vertexCount = m.vertexCount;
				Vector3[] vertices = m.vertices;
				await Task.Run(() => Wound(point, intensity * .5f, vertexCount, vertices, vc, wd));
				
				character.sharedMesh.SetColors(vc);
			}
		}
		/// <summary>
		/// Keeps the vertex related arrays cached between processing 'hits'.
		/// Only has one wound that's calculated from a point, with no directional data.
		/// Cannot activate dismemberment.
		/// Does not activate bleeding.
		/// 
		/// Useful for explosions.
		/// </summary>
		/// <param name="points">Hit points.</param>
		/// <param name="intensities">A 2000th of the damages you wish to deal to the character's health parametre.</param>
		/// <param name="rbodies">Rigidbodies of the limbs that were hit, as this system is made for active ragdolls.</param>
		public virtual async void BluntWound(Vector3[] points, float[] intensities, Rigidbody[] rbodies)
		{
			foreach (SkinnedMeshRenderer character in characterRenderers)
			{
				Mesh m = new Mesh();
				character.BakeMesh(m);
				
				List<Color> vc = new List<Color>();
				character.sharedMesh.GetColors(vc);
				if (vc.Count == 0)
				{
					vc = new List<Color>(new Color[m.vertexCount]);
					for (int i = vc.Count - 1; i >= 0; i--)
						vc[i] = Color.white;
				}
				
				//Deal with each wound using the same baked mesh
				for (int i = points.Length; i >= 0; i--)
				{
					Vector3 point = points[i];
					Rigidbody rbody = rbodies[i];
					Vector3 direction = (rbody.position - point).normalized;
					float intensity = intensities[i];
					
					pm.EmitAt(point, Quaternion.identity, (int)ParticleType.smearer, 1, rbody.transform);
					
					pm.EmitAt(point, Quaternion.LookRotation(direction), (int)ParticleType.droplet, (int)(Random.Range(0, intensity * 2000 + 2)));
					
					#if USE_GARGGORE_DAMAGE_SYSTEM
					float sqrInt = intensity*intensity*9 * 2000;
					health -= (byte)Mathf.Clamp(sqrInt, 0, health * 1.0f);
					#if USE_GARGGORE_BLEEDING_SYSTEM
					bloodVolume -= (ushort)Mathf.Clamp(sqrInt, 0, bloodVolume * 1.0f);
					#endif
					rbody.AddForceAtPosition(direction * sqrInt * 24, point);
					#endif
					
					if (health < 75)
						pm.EmitAt(point, Quaternion.LookRotation(direction), (int)ParticleType.gibFlesh, (int)(Random.Range(0, intensity * 500 + 2)));
					
					
					
					Vector3 ogPoint = point; point = ct.InverseTransformPoint(point);
					
					int vertexCount = m.vertexCount;
					Vector3[] vertices = m.vertices;
					await Task.Run(() => Wound(point, intensity * .5f, vertexCount, vertices, vc));
				}
				
				character.sharedMesh.SetColors(vc);
			}
		}
		
		public static float NonEuclideanSqrMagnitude(Vector3 v)
		{
			return v.x*v.x + v.y*v.y + v.z*v.z; //Square it so that the end result is always positive
		}
		System.Random randy;
		protected float RandomRange(float min, float max)
		{
			return (float)(min + randy.NextDouble() * (max - min));
		}
	}
}