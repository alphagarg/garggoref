﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GargGoref
{
	public class CameraFollow : MonoBehaviour
	{
		public Transform target;
		Transform t;
		
		void FixedUpdate()
		{
			if (!t)
				t = transform;
			
			t.position = t.position * .9f + (target.position - t.forward * 2) * .1f;
		}
		
		void Update()
		{
			if (Input.GetKey(KeyCode.Mouse1))
				t.eulerAngles = new Vector3
					(
						Mathf.Clamp(-Input.GetAxis("Mouse Y") * 4 + (t.eulerAngles.x > 180 ? t.eulerAngles.x - 360 : t.eulerAngles.x), -89, 89),
						t.eulerAngles.y + Input.GetAxis("Mouse X") * 4,
						Input.GetAxis("Mouse X") * 4
					);
		}
	}
}