﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GargGoref
{
	public class ClickityShooty : MonoBehaviour
	{
		public UnityEngine.UI.Text txt;
		public ExampleImplementation ei;
		//Damage dealt to character health
		public float damagePcnt = 48;
		void Update()
		{
			if (Input.GetButtonDown("Fire1") && Camera.main)
			{
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if (Physics.Raycast(ray, out hit, 65535, (1 << 8)))
					ei.Damage(hit.point, ray.direction, damagePcnt, hit.rigidbody);
			}
			txt.text = ei.bhl.bloodVolume + "ml of blood | " + ei.bhl.health + "% HP | " + ei.bhl.bleeding.Count + " points of bleeding";
		}
		public void SetSpeed(float value)
		{
			damagePcnt = value;
		}
		public void Reset()
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene(0);
		}
	}
}