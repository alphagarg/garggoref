﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GargGoref
{
	public class ExampleImplementation : MonoBehaviour
	{
		public BloodyHellLad bhl;
		
		//Used to prevent limb collider intercollision.
		public Collider[] cols;
		//Used to enable the ragdoll later
		public Rigidbody[] rbs;
		void Start()
		{
			foreach (var e in rbs)
				e.isKinematic = true;
			
			foreach (var e in cols)
				foreach (var e2 in cols)
					Physics.IgnoreCollision(e, e2, true);
		}
		void Update()
		{
			if (bhl.health == 0)
				foreach (var e in rbs)
					e.isKinematic = false;
		}
		public void Damage(Vector3 point, Vector3 direction, float damage, Rigidbody rbody)
		{
			//Ouchies!
			OnDamage(point, direction, damage, rbody);
		}
		protected virtual void OnDamage(Vector3 point, Vector3 direction, float damage, Rigidbody rbody)
		{
			//2000th of the damage percentage value
			bhl.VelocityWound(point, direction, damage * .0005f, rbody);
		}
	}
}