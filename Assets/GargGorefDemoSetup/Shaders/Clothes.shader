﻿Shader "Custom/Clothes"
{
    Properties
    {
        _Cutoff ("Alpha cutoff", Range(0,1)) = 0.25
        _MainTex ("Mask", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue"="AlphaTest" "RenderType"="TransparentCutout" }
        Cull Off

        CGPROGRAM
        #pragma surface surf Lambert vertex:vert fullforwardshadows alphatest:_Cutoff
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
			float4 color;
        };

		void vert (inout appdata_full v, out Input OUT)
		{
            OUT.uv_MainTex = v.texcoord;
            OUT.color = v.color;
		}
        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            
            half a = IN.color.a;
            
            o.Albedo = a;
            
            a *= a;
            a *= a;
            a *= a;
            a *= a;
            
            o.Alpha = (c.r + a) * .501;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
