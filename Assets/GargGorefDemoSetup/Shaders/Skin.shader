﻿Shader "Custom/Skin"
{
    Properties
    {
        _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        _MainTex ("Mask", 2D) = "white" {}
        _Gore ("Gore", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue"="AlphaTest" "RenderType"="TransparentCutout" }
        Cull Off

        CGPROGRAM
        #pragma surface surf Standard vertex:vert fullforwardshadows alphatest:_Cutoff
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _Gore;

        struct Input
        {
            float2 uv_MainTex;
			float4 color;
        };

		void vert (inout appdata_full v, out Input OUT)
		{
            OUT.uv_MainTex = v.texcoord;
            OUT.color = v.color;
		}
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            
            o.Albedo = lerp(tex2D (_Gore, IN.uv_MainTex).rgb, fixed3(.8, .7, .6), (IN.color.a - .75) * 4);
            o.Alpha = (c.r + IN.color.a) * .501;
            o.Smoothness = saturate(1 - IN.color.a * 4);
            o.Metallic = saturate(1 - IN.color.a * 4);
        }
        ENDCG
    }
    FallBack "Diffuse"
}
